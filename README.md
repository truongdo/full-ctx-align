# Introduction #
This repository demonstrate how to get full-context alignment from speech and text.

1. Step 1, prepare data with the same format with `data`.
2. Step 2, run `run.sh`
3. Results will be written in `output/est/labels/full_rev_align`

# Advanced configuration #
## Speech parameters extraction

```bash
mkdir conf

# Make config file
cat <<EOF > conf/Config.cfg
# Configuration
declare -A maxf0
declare -A minf0
minf0=( ["*bdl*"]="40" ["*clb*"]="130" ["*jmk*"]="50" ["*rms*"]="40" ["*slt*"]="110" )
maxf0=( ["*bdl*"]="210" ["*clb*"]="260" ["*jmk*"]="180" ["*rms*"]="200" ["*slt*"]="280" )

file_minf0="110"  # default minf0
file_maxf0="280" # default maxf0

function getF0Range(){
  filename="$1"
  for spk in "${!maxf0[@]}"; do
      if [[ `echo $filename | grep -E "$spk"` ]]; then
          file_minf0=${minf0[$spk]}
          file_maxf0=${maxf0[$spk]}
          break
      fi
  done
}

fl=400  # frame length in point (400 = 16000 * 0.025)
fs=80  # Frame shift in point (80 = 16000 * 0.005)

analyzer="world"  # sptk | world
SAMPFREQ=16000
SAMPKHZ=16  # `echo $SAMPFREQ | x2x +af | sopr -m 0.001 | x2x +fa`
FFTLEN=2048
FREQWARP=0.42  # for 8Khz: 0.31, 10Khz: 0.35, 12Khz: 0.37, 16Khz: 0.42, 22,05Khz: 0.45, 32Khz: 0.45, 44.1Khz: 0.53, 48Khz: 0.55
WINDOWTYPE=1 # Window type -> 0: Blackman 1: Hamming 2: Hanning
NORMALIZE=1  # # Normalization -> 0: none  1: by power  2: by magnitude

NMGCWIN=3     # DO NOT EDIT THIS LINE UNLESS YOU KNOW WHAT YOU ARE REALLY DOING
NLF0WIN=3     # DO NOT EDIT THIS LINE UNLESS YOU KNOW WHAT YOU ARE REALLY DOING
NBAPWIN=3     # DO NOT EDIT THIS LINE UNLESS YOU KNOW WHAT YOU ARE REALLY DOING

MGCORDER=39
NUM_THREAD=4

MGCDIM=`expr $MGCORDER + 1`
LF0DIM=1
BAPDIM=1

MGCWINDIM=`expr $NMGCWIN \* $MGCDIM`
LF0WINDIM=`expr $NLF0WIN \* $LF0DIM`
BAPWINDIM=`expr $NBAPWIN \* $BAPDIM`
EOF

# Run hmm-extract-params with new config file
docker run --rm -v `pwd`/conf:/extr-data/conf -v `pwd`/output:/data-tts -v `pwd`/data:/data \
           ahclab/hmm-extract-params:latest run 8

```
           
           