#! /bin/bash
#
# run.sh
# Copyright (C) 2017 truongdo <truongdq54@gmail.com>
#
# Distributed under terms of the MIT license.
#


mkdir -p output
docker run --rm -v `pwd`/output:/data-tts -v `pwd`/data:/data \
           ahclab/hmm-extract-params:latest run 8

cp -rf data/txt output

docker run --rm  \
    -v `pwd`/output/est:/project/est \
    -v `pwd`/Config.pm:/src/align/Config.pm \
    -v `pwd`/output:/data \
    --rm ahclab/hmm-makelab-pause-est letsdoit
