#################################################################
#                           Config.pm                           #
#################################################################
#             Coded by Shinnosuke Takamichi (NAIST)             #
#                           2012.10.21                          #
#################################################################
# This program defines configuration variables.                 #
#################################################################

# Settings ==============================
@SET        = ('cmp');
@cmp        = ('mgc', 'lf0', 'bap');
$ref{'cmp'} = \@cmp;

%vflr       = ('mgc' => "0.01", 'lf0' => "0.01", 'bap' => "0.01"); # variance flooring
%strb       = ('mgc' => "1", 'lf0' => "2", 'bap' => "5");    # stream start
%stre       = ('mgc' => '1', 'lf0' => "4", 'bap' => "7");    # stream end
%msdi       = ('mgc' => '0', 'lf0' => "1", 'bap' => "1");    # msd information
%strw       = ('mgc' => '1.0', 'lf0' => "0.0", 'bap' => "0.0");  # stream weights
%ordr       = ('mgc' => '40', 'lf0' => "1", 'bap' => "1");   # feature order
%nwin       = ('mgc' => '3', 'lf0' => "3", 'bap' => "3");    # number of windows

# Global Value ==========================
$nState     = 5;                 # number of states
$nIte       = 5;                 # number of iterations for embedded training
$beam       = "1000 100 5000";   # initial, inc, and upper limit of beam width
$wf         = 3;                 # mixture weight flooring
$nMix       = 4;                 # number of mixtures for mgc
$mindur     = 49;                # minimum duration of short pause
$pau        = ", ";              # pause symbol

# Environments ==============================
$prjdir     = '/project/est';
$datdir     = "/data";
$srcdir = "/src/scripts";
$DIR{'txt'} = "$datdir/txt";      # *.txt file (please fix as you want)

# HTS commands ==============================
$HCOMPV     = "HCompV";
$HEREST     = "HERest";
$HHED       = "HHEd";
$HVITE      = "HVite";
$HPARSE     = "HParse";

# other scripts
$MKNET      = "$srcdir/align/mknet.sh";
#$MKNET      = "$srcdir/align/mknet_ja.sh";  # This is used for Japanese
$EXTLAB_SP  = "$srcdir/align/ext_lab_sp.awk";
$EXTLAB_V  = "$srcdir/align/ext_lab_v.awk";
$ADDPAU     = "$srcdir/align/addpau.sh";
#$ADDPAU     = "$srcdir/align/addpau_jp.sh";
$TRACE2TEXT = "$srcdir/txt/Trace2Text.pl";

# Parallel training
$parallel = 1;
$nj = 4;
$split = "/src/split.py";
eval {require Parallel::ForkManager};
if($@) { $parallel = 0; print "Module Parallel::ForkManager is not installed\n using only single cpu for training\n";}
else { print "Using parallel training with $nj cpus\n"; }

# Switch ================================
$MKEMV = 1;  # preparing environments
$HCMPV = 1;  # computing a global variance
$INIT  = 1;  # initialization & reestimation
$MMMMF = 1;  # making a monophone mmf
$ERST0 = 1;  # embedded reestimation (monophone)
$VITE0 = 1;  # pause detection with monophone
$ERST1 = 1;  # embedded reestimation (refined monophone)
$VITE1 = 1;  # pause detection with refined monophone
$ERST2 = 1;  # embedded reestimation (refined monophone 1mix)
$UPMIX = 1;  # increasing the number of mixture components (1mix -> 16mix)
$ERST3 = 1;  # embedded reestimation (monophone 16mix)
$VITE2 = 1;  # pause detection with monophone 16mix
$ERST4 = 1;  # embedded reestimation (refined monophone 16mix)
$VITE3 = 1;  # pause detection with refined monophone 16mix

1;
